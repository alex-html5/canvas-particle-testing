const RADIUS = 10;
const PARTICLES = [];
const MESSAGE = "Click and drag to launch a particle.";

const getScreenHeight = () => window.innerHeight;
const getScreenWidth = () => window.innerWidth;

const canvas = document.createElement("canvas");
const context = canvas.getContext("2d");
document.body.appendChild(canvas);

const fillBackground = (drawCtx) => {
  drawCtx.fillStyle = 'black';
  drawCtx.fillRect(0, 0, getScreenWidth(), getScreenHeight());

  // Only render instructions when nothing is moving.
  if (PARTICLES.length === 0) {
    drawCtx.fillStyle = 'white';
    drawCtx.font = '12px Arial';
    drawCtx.fillText(MESSAGE, 10, 25);
  }
}

/**
 * Represents a single "particle" bouncing around the screen.
 */
class Particle {
  centerX;
  centerY;
  movX;
  movY;
  color;

  constructor(posX, posY, movX, movY) {
    this.centerX = posX;
    this.centerY = posY;
    this.movX = movX;
    this.movY = movY;
    this.color = 'white';
  }

  move () {
    // Out of bounds Left / Right
    if ((this.centerX - (RADIUS)) <= 0) {
      //this.centerX = RADIUS;
      if (this.centerX + this.movX < this.centerX) {
        this.movX *= -1;
      }
    } else if ((this.centerX + (RADIUS)) >= getScreenWidth()) {
      //this.centerX = getScreenWidth() - RADIUS;
      if (this.centerX + this.movX > this.centerX) {
        this.movX *= -1;
      }
    }

    // Out of bounds Top / Bottom
    if ((this.centerY - (RADIUS)) <= 0) {
      //this.centerY = RADIUS;
      if (this.centerY + this.movY < this.centerY) {
        this.movY *= -1;
      }
    } else if ((this.centerY + (RADIUS)) >= getScreenHeight()) {
      //this.centerY = getScreenHeight() - RADIUS;
      if (this.centerY + this.movY > this.centerY) {
        this.movY *= -1;
      }
    }

    this.centerX += this.movX;
    this.centerY += this.movY;
  }

  draw (drawCtx) {
    drawCtx.beginPath();
    drawCtx.fillStyle = this.color;
    drawCtx.arc(this.centerX, this.centerY, RADIUS, 0, Math.PI*2, true);
    drawCtx.closePath();
    drawCtx.fill();
  }

}

/**
 * Represents the line drawn by a user to launch a "particle".
 */
class LaunchLine {
  isDrawing;
  mouseDownX;
  mouseDownY;
  mouseX;
  mouseY;

  startDrawing (startX, startY) {
    this.isDrawing = true;
    this.mouseX = this.mouseDownX = startX;
    this.mouseY = this.mouseDownY = startY;
  }

  updateMousePos (mousePosX, mousePosY) {
    if (this.isDrawing) {
      this.mouseX = mousePosX;
      this.mouseY = mousePosY;
    }
  }

  calculateHorizontalMovement () {
    let hLineDist;
    let leftToRight = false;
    if (this.mouseX > this.mouseDownX) {
      hLineDist = this.mouseX - this.mouseDownX;
      leftToRight = true;
    } else {
      hLineDist = this.mouseDownX - this.mouseX;
    }
  }

  launchParticle () {
    const movX = ((this.mouseX - this.mouseDownX) * -1) / 20;
    const movY = ((this.mouseY - this.mouseDownY) * -1) / 20;
    return new Particle(this.mouseX, this.mouseY, movX, movY);
  }

  reset () {
    this.isDrawing = false;
    this.mouseDownX = this.mouseDownY = this.mouseX = this.mouseY = -1;
  }

  draw (drawCtx) {
    if (this.isDrawing) {
      drawCtx.beginPath();
      drawCtx.strokeStyle = 'red';
      drawCtx.lineWidth = 1;
      drawCtx.moveTo(this.mouseDownX, this.mouseDownY);
      drawCtx.lineTo(this.mouseX, this.mouseY);
      drawCtx.stroke();
      drawCtx.closePath();
    }
  }
}

const launchLine = new LaunchLine();

canvas.addEventListener('mousedown', e => {
  console.log(`MouseDown: x:${e.offsetX}, y:${e.offsetY}`);
  launchLine.startDrawing(e.offsetX, e.offsetY);
});

canvas.addEventListener('mousemove', e => {
  launchLine.updateMousePos(e.offsetX, e.offsetY)
});

canvas.addEventListener('mouseup', e => {
  PARTICLES.push(launchLine.launchParticle());
  launchLine.reset();
});

window.addEventListener('mouseout', e => {
  launchLine.reset();
});

const onResize = () => {
  canvas.width = getScreenWidth();
  canvas.height = getScreenHeight();
  fillBackground(context);
}

const run = () => {
  window.addEventListener('resize', onResize, false);
  onResize();

  setInterval(() => {
    fillBackground(context);
    launchLine.draw(context);
    PARTICLES.forEach((p) => {
      p.move();
      p.draw(context);
    });  
  }, 30);
}

run();
